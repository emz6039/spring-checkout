import os
import subprocess

def read_and_remove_containers():
    result = subprocess.run("docker image ls -a -q".split(" "), capture_output=True, text=True).stdout
    result = result.strip().split("\n")
    for item in result:
        subprocess.run(f"docker image rm -f {item}".split(" "))


def main():
    read_and_remove_containers()

if __name__ == '__main__':
    main()
