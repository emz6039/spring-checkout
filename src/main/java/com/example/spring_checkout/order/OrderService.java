package com.example.spring_checkout.order;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public List<Order> getOrders(){
        return orderRepository.findAll();
    }

    public Long createOrder(Order order){
//        Optional<Order> orderOptional = orderRepository.findById(order.getId());
//        if(orderOptional.isPresent()){
//            throw new IllegalStateException("Order Already Taken");
//        }
        orderRepository.save(order);
        return order.getId();
    }

    public void deleteOrder(Order order){
        Optional<Order> orderOptional = orderRepository.findById(order.getId());
        if(orderOptional.isEmpty()) {
            throw new IllegalStateException("Order does not exists: "+order.getId());
        }
        orderRepository.delete(order);
    }
}
