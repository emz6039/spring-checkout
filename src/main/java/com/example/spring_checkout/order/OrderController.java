package com.example.spring_checkout.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path="api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping(path = "/")
    public List<Order> getOrders(){
        return orderService.getOrders();
    }

    @PostMapping(path = "/")
    public Long createOrder (@RequestBody Order order) throws Exception {
        return orderService.createOrder(order);
    }
}
