package com.example.spring_checkout.order;

import jakarta.persistence.*;

import java.util.List;

//
@Entity
@Table(name="spring_order")
public class Order {

    @Id
    @SequenceGenerator(
            name = "order_sequence",
            sequenceName =  "order_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "order_sequence"
    )
    private Long id;
    private String email; // additionally username as well
    private String products;
    private float total;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
