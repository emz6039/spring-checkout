package com.example.spring_checkout.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
    @Query("select s from Order s where s.email = ?1")
    Optional<Order> findOrderByEmail (String order);
}

