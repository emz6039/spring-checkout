package com.example.spring_checkout.order;

import org.aspectj.weaver.ast.Or;

// How to create multiple order exceptions
public class OrderException extends Exception{
   // denotes that something went wrong with order processor
    public OrderException(String errorMsg){
        super(errorMsg);
    }

}
