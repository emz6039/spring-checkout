package com.example.spring_checkout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCheckoutApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCheckoutApplication.class, args);
    }

}
