#FROM eclipse-temurin:17-jdk-alpine
#VOLUME /tmp
#COPY ./target/*.jar app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]

FROM maven:3.9.5-amazoncorretto-17-debian AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN java -version
RUN mvn -e -f /usr/src/app/pom.xml clean package

FROM amazoncorretto:17-alpine3.18
COPY --from=build /usr/src/app/target/spring_checkout-0.0.1-SNAPSHOT.jar /usr/app/spring_checkout-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/spring_checkout-0.0.1-SNAPSHOT.jar"]